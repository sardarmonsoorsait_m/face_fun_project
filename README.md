**Dependencies**
    - Python 3.6.9
    -  keras 2.4.3
    -  cv2 4.1.2
    -  dlib 19.18.0
    -  cmake 3.12.0
    -  flask 1.1.2


**Demo video**
 - [Demo video](https://youtu.be/_9xjI6yOnPc)

 **OverView**
This is a fun app when you upload photo via choosefileand press predict button
our cnn model compare your face with some actors and find which actor you are more similar with.
then swap your face with that actor's famous image
**How to run asaiTech Face Fun app in Google colab using ngrok**

  1. Download the repository to local system
  2. Upload the repository to your google Drive
  3. In colab open face_app_flask_ngrok.ipynb Using File>open notebook>GoogleDrive
  4. then, in code cell type cd drive/Mydrive/face_fun_project and run to navigate to our         folderjust like terminal
  5. then Run cells one by one, after run last cell you will get three links of our face fun app  
