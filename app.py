#Import necessary libraries
from flask import Flask, render_template, request

import numpy as np
import os
import keras
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import cv2

#load model
model =load_model("model/simple_face_fun_model.h5")

print('@@ Model loaded')



def pred_face(face):    
	font = cv2.FONT_HERSHEY_SIMPLEX
	fontscale=.5
	color=[0,0,255]
	face_cascade = cv2.CascadeClassifier('model/haarcascade_frontalface_default.xml')
	org=(1,1)
	color_img = cv2.imread(face)
	grey_img = cv2.cvtColor(color_img,cv2.COLOR_BGR2GRAY)
	faces =  face_cascade.detectMultiScale(grey_img,1.1,12)
	img_count = 0
	for (x,y,w,h) in faces:
	  img_count+=1
	  org=(x+10,y+10)
	  face_img = color_img[y:y+h,x:x+w]
	  cv2.imwrite('static/user uploaded/%dimg.jpg'%(img_count),face_img)
	  img = load_img('static/user uploaded/%dimg.jpg'%(img_count),target_size=(227,227))
	  img= img_to_array(img)/255
	  img=np.expand_dims(img,axis=0)
	  pred_prob = model.predict(img).round(3)
	  pred = np.argmax(pred_prob)
	  print(pred_prob)
	  cv2.rectangle(color_img,(x,y),(x+w,y+h),[255,0,0],3)
	  cv2.imwrite(face,color_img)
	  if pred ==0:


	    return "You are like Ultimate star..."+"("+str(pred_prob[0][0])+")", 'ajith.html'		  
	  elif pred ==1:
 
		  return "you are like Ulaga nayagan"+"("+str(pred_prob[0][1])+")", 'kamal.html'		
         

	  elif pred ==2:

          		  
		  return "you are like rajini"+"("+str(pred_prob[0][2])+")", 'rajini.html'          
	  else:
    
	    return "you are like ilayathalapathi"+"("+str(pred_prob[0][3])+")", 'vijay.html'


#------------>>pred_cot_dieas<<--end
    

# Create flask instance
app = Flask(__name__)

# render index.html page
@app.route("/", methods=['GET', 'POST'])
def home():
        return render_template('index.html')
    
 
# get input image from client then predict class and render respective .html page for solution
@app.route("/predict", methods = ['GET','POST'])
def predict():
     if request.method == 'POST':
        file = request.files['image'] # fet input
        filename = file.filename        
        print("@@ Input posted = ", filename)
        
        file_path = os.path.join('static/user uploaded', filename)
        file.save(file_path)

        print("@@ Predicting class......")
        pred, output_page = pred_face(face=file_path)
              
        return render_template(output_page, pred_output = pred, user_image = file_path)
    
# For local system & cloud
if __name__ == "__main__":
    app.run(threaded=False,debug=True) 
    
    
